<?php

namespace App\Form;

use App\Entity\Prenom;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrenomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('prenom', EntityType::class, [
                'class' => Prenom::class,
                'choice_label' => 'prenom',
                'required' => false,
                'placeholder' => 'Choose or type a new firstname',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Prenom::class,
        ]);
    }
}
