<?php

namespace App\Controller\Admin;

use App\Entity\Relation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;

class RelationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Relation::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            AssociationField::new('premier_individu'),
            AssociationField::new('deuxieme_individu'),
            AssociationField::new('type_relation'),
            IntegerField::new('ordre'),
        ];
    }
}
