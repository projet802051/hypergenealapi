<?php

namespace App\Controller\Admin;

use App\Entity\Commune;
use App\Entity\Evenement;
use App\Entity\Individu;
use App\Entity\Media;
use App\Entity\Nom;
use App\Entity\Prenom;
use App\Entity\Relation;
use App\Entity\Sexe;
use App\Entity\TypeEvenement;
use App\Entity\TypeRelation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(IndividuCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('HyperGenealApi');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Individus', 'fas fa-user-group', Individu::class);
        yield MenuItem::linkToCrud('Évènements', 'fas fa-star', Evenement::class);
        yield MenuItem::linkToCrud('Communes', 'fas fa-location-dot', Commune::class);
        yield MenuItem::linkToCrud('Medias', 'fas fa-images', Media::class);
        yield MenuItem::linkToCrud('Noms', 'fas fa-address-card', Nom::class);
        yield MenuItem::linkToCrud('Prénoms', 'fa fa-address-card', Prenom::class);
        yield MenuItem::linkToCrud('Relations', 'fas fa-people-arrows', Relation::class);
        yield MenuItem::linkToCrud('Sexes', 'fas fa-venus-mars', Sexe::class);
        yield MenuItem::linkToCrud("Types d'évènements", 'fas fa-star', TypeEvenement::class);
        yield MenuItem::linkToCrud('Types de relations', 'fas fa-people-arrows', TypeRelation::class);
    }
}
