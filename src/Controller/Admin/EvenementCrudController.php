<?php

namespace App\Controller\Admin;

use App\Entity\Evenement;
use App\Form\MediaType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class EvenementCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Evenement::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            DateField::new('date_evenement'),
            AssociationField::new('type_evenement'),
            AssociationField::new('commune'),
            CollectionField::new('medias')
            ->setEntryType(MediaType::class)
            ->allowAdd()
            ->allowDelete(),
        ];
    }
}
