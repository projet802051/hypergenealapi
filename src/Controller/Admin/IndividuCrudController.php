<?php

namespace App\Controller\Admin;

use App\Entity\Individu;
use App\Form\EvenementType;
use App\Form\MediaType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class IndividuCrudController extends AbstractCrudController
{
    public function configureActions(Actions $actions): Actions
    {
        $actions->add(Crud::PAGE_INDEX, Action::DETAIL);

        return $actions;
    }

    public static function getEntityFqcn(): string
    {
        return Individu::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->hideOnForm();
        yield BooleanField::new('prive', 'Privé');

        yield AssociationField::new('nom', 'Nom')
            ->autocomplete()
            ->setSortProperty('nom');

        yield AssociationField::new('prenoms', 'Prénoms')
            ->hideOnIndex()
            ->hideOnDetail();
        yield CollectionField::new('prenoms', 'Prénoms')
            ->hideOnForm();

        yield AssociationField::new('sexe', 'Sexe');

        yield CollectionField::new('evenements', 'Événements')
            ->hideOnForm();
        yield CollectionField::new('evenements', 'Événements')
            ->setFormTypeOption('entry_type', EvenementType::class)
            ->setEntryIsComplex(true)
            ->onlyOnForms();

        yield CollectionField::new('medias', 'Médias')
            ->setEntryType(MediaType::class)
            ->allowAdd()
            ->allowDelete();

        return parent::configureFields($pageName);
    }
}
