<?php

namespace App\Entity;

use App\Repository\RelationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RelationRepository::class)]
class Relation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $ordre = 0;

    #[ORM\ManyToOne(inversedBy: 'relations_first')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Individu $premier_individu = null;

    #[ORM\ManyToOne(inversedBy: 'relations_second')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Individu $deuxieme_individu = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeRelation $type_relation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): static
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getPremierIndividu(): ?Individu
    {
        return $this->premier_individu;
    }

    public function setPremierIndividu(?Individu $premier_individu): static
    {
        $this->premier_individu = $premier_individu;

        return $this;
    }

    public function getDeuxiemeIndividu(): ?Individu
    {
        return $this->deuxieme_individu;
    }

    public function setDeuxiemeIndividu(?Individu $deuxieme_individu): static
    {
        $this->deuxieme_individu = $deuxieme_individu;

        return $this;
    }

    public function getTypeRelation(): ?TypeRelation
    {
        return $this->type_relation;
    }

    public function setTypeRelation(?TypeRelation $type_relation): static
    {
        $this->type_relation = $type_relation;

        return $this;
    }
}
