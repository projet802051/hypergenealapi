<?php

namespace App\Entity;

use App\Repository\EvenementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EvenementRepository::class)]
class Evenement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_evenement = null;

    #[ORM\ManyToOne(inversedBy: 'evenements')]
    private ?Commune $commune = null;

    #[ORM\ManyToOne(inversedBy: 'evenements')]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeEvenement $type_evenement = null;

    #[ORM\OneToMany(targetEntity: Media::class, mappedBy: 'evenement')]
    private Collection $medias;

    #[ORM\ManyToMany(targetEntity: Individu::class, mappedBy: 'evenements')]
    private Collection $individus;

    public function __construct()
    {
        $this->medias = new ArrayCollection();
        $this->individus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateEvenement(): ?\DateTimeInterface
    {
        return $this->date_evenement;
    }

    public function setDateEvenement(\DateTimeInterface $date_evenement): static
    {
        $this->date_evenement = $date_evenement;

        return $this;
    }

    public function getCommune(): ?Commune
    {
        return $this->commune;
    }

    public function setCommune(?Commune $commune): static
    {
        $this->commune = $commune;

        return $this;
    }

    public function getTypeEvenement(): ?TypeEvenement
    {
        return $this->type_evenement;
    }

    public function setTypeEvenement(?TypeEvenement $type_evenement): static
    {
        $this->type_evenement = $type_evenement;

        return $this;
    }

    /**
     * @return Collection<int, Media>
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Media $media): static
    {
        if (!$this->medias->contains($media)) {
            $this->medias->add($media);
            $media->setEvenement($this);
        }

        return $this;
    }

    public function removeMedia(Media $media): static
    {
        if ($this->medias->removeElement($media)) {
            // set the owning side to null (unless already changed)
            if ($media->getEvenement() === $this) {
                $media->setEvenement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Individu>
     */
    public function getIndividus(): Collection
    {
        return $this->individus;
    }

    public function addIndividu(Individu $individu): static
    {
        if (!$this->individus->contains($individu)) {
            $this->individus->add($individu);
            $individu->addEvenement($this);
        }

        return $this;
    }

    public function removeIndividu(Individu $individu): static
    {
        if ($this->individus->removeElement($individu)) {
            $individu->removeEvenement($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this
            ->getTypeEvenement()
            ->getNom()
            .' : '.
            $this->getDateEvenement()->format('d-m-Y')
            .($this->getCommune() ? ' à '.$this->getCommune()->getNom() : '');
    }
}
