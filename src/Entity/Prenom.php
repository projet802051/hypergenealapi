<?php

namespace App\Entity;

use App\Repository\PrenomRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PrenomRepository::class)]
class Prenom
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $prenom = null;

    #[ORM\ManyToMany(targetEntity: Individu::class, mappedBy: 'prenoms')]
    private Collection $individus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection<int, Individu>
     */
    public function getIndividus(): Collection
    {
        return $this->individus;
    }

    public function addIndividu(Individu $individu): static
    {
        if (!$this->individus->contains($individu)) {
            $this->individus->add($individu);
            $individu->addPrenom($this);
        }

        return $this;
    }

    public function removeIndividu(Individu $individu): static
    {
        if ($this->individus->removeElement($individu)) {
            $individu->removePrenom($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getPrenom();
    }
}
