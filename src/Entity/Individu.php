<?php

namespace App\Entity;

use App\Repository\IndividuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IndividuRepository::class)]
class Individu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?bool $prive = null;

    #[ORM\ManyToOne]
    private ?Sexe $sexe = null;

    #[ORM\ManyToOne(inversedBy: 'individus')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Nom $nom = null;

    #[ORM\ManyToMany(targetEntity: Prenom::class, inversedBy: 'individus', cascade: ['persist'])]
    private Collection $prenoms;

    #[ORM\ManyToMany(targetEntity: Evenement::class, inversedBy: 'individus', cascade: ['persist'])]
    private Collection $evenements;

    #[ORM\OneToMany(targetEntity: Media::class, mappedBy: 'individu', orphanRemoval: true, cascade: ['persist'])]
    private Collection $medias;

    #[ORM\OneToMany(targetEntity: Relation::class, mappedBy: 'premier_individu')]
    private Collection $relations_first;

    #[ORM\OneToMany(targetEntity: Relation::class, mappedBy: 'deuxieme_individu')]
    private Collection $relations_second;

    public function __construct()
    {
        $this->prenoms = new ArrayCollection();
        $this->evenements = new ArrayCollection();
        $this->medias = new ArrayCollection();
        $this->relations_first = new ArrayCollection();
        $this->relations_second = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isPrive(): ?bool
    {
        return $this->prive;
    }

    public function setPrive(bool $prive): static
    {
        $this->prive = $prive;

        return $this;
    }

    public function getSexe(): ?Sexe
    {
        return $this->sexe;
    }

    public function setSexe(?Sexe $sexe): static
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getNom(): ?Nom
    {
        return $this->nom;
    }

    public function setNom(?Nom $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Prenom>
     */
    public function getPrenoms(): Collection
    {
        return $this->prenoms;
    }

    public function addPrenom(Prenom $prenom): static
    {
        if (!$this->prenoms->contains($prenom)) {
            $this->prenoms->add($prenom);
        }

        return $this;
    }

    public function removePrenom(Prenom $prenom): static
    {
        $this->prenoms->removeElement($prenom);

        return $this;
    }

    /**
     * @return Collection<int, Evenement>
     */
    public function getEvenements(): Collection
    {
        return $this->evenements;
    }

    public function addEvenement(Evenement $evenement): static
    {
        if (!$this->evenements->contains($evenement)) {
            $this->evenements->add($evenement);
        }

        return $this;
    }

    public function removeEvenement(Evenement $evenement): static
    {
        $this->evenements->removeElement($evenement);

        return $this;
    }

    /**
     * @return Collection<int, Media>
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Media $media): static
    {
        if (!$this->medias->contains($media)) {
            $this->medias->add($media);
            $media->setIndividu($this);
        }

        return $this;
    }

    public function removeMedia(Media $media): static
    {
        if ($this->medias->removeElement($media)) {
            // set the owning side to null (unless already changed)
            if ($media->getIndividu() === $this) {
                $media->setIndividu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Relation>
     */
    public function getRelationsFirst(): Collection
    {
        return $this->relations_first;
    }

    public function addRelationsFirst(Relation $relationsFirst): static
    {
        if (!$this->relations_first->contains($relationsFirst)) {
            $this->relations_first->add($relationsFirst);
            $relationsFirst->setPremierIndividu($this);
        }

        return $this;
    }

    public function removeRelationsFirst(Relation $relationsFirst): static
    {
        if ($this->relations_first->removeElement($relationsFirst)) {
            // set the owning side to null (unless already changed)
            if ($relationsFirst->getPremierIndividu() === $this) {
                $relationsFirst->setPremierIndividu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Relation>
     */
    public function getRelationsSecond(): Collection
    {
        return $this->relations_second;
    }

    public function addRelationsSecond(Relation $relationsSecond): static
    {
        if (!$this->relations_second->contains($relationsSecond)) {
            $this->relations_second->add($relationsSecond);
            $relationsSecond->setDeuxiemeIndividu($this);
        }

        return $this;
    }

    public function removeRelationsSecond(Relation $relationsSecond): static
    {
        if ($this->relations_second->removeElement($relationsSecond)) {
            // set the owning side to null (unless already changed)
            if ($relationsSecond->getDeuxiemeIndividu() === $this) {
                $relationsSecond->setDeuxiemeIndividu(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        $prenomArray = $this->getPrenoms()->map(function ($prenom) {
            return $prenom->getPrenom();
        });

        $prenomString = '';
        foreach ($prenomArray as $prenom) {
            $prenomString .= ($prenomString ? ', ' : '').$prenom;
        }

        return $this->getNom()->getNom().' '.$prenomString;
    }
}
