<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240214120241 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création de la base';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE commune (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, code_insee VARCHAR(5) DEFAULT NULL, code_postal VARCHAR(5) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE evenement (id INT AUTO_INCREMENT NOT NULL, date_evenement DATE NOT NULL, commune_id INT DEFAULT NULL, type_evenement_id INT NOT NULL, INDEX IDX_B26681E131A4F72 (commune_id), INDEX IDX_B26681E88939516 (type_evenement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE individu (id INT AUTO_INCREMENT NOT NULL, prive TINYINT(1) NOT NULL, sexe_id INT DEFAULT NULL, nom_id INT NOT NULL, INDEX IDX_5EE42FCE448F3B3C (sexe_id), INDEX IDX_5EE42FCEC8121CE9 (nom_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE individu_prenom (individu_id INT NOT NULL, prenom_id INT NOT NULL, INDEX IDX_C3483963480B6028 (individu_id), INDEX IDX_C348396358819F9E (prenom_id), PRIMARY KEY(individu_id, prenom_id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE individu_evenement (individu_id INT NOT NULL, evenement_id INT NOT NULL, INDEX IDX_9F40312480B6028 (individu_id), INDEX IDX_9F40312FD02F13 (evenement_id), PRIMARY KEY(individu_id, evenement_id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, chemin VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, evenement_id INT DEFAULT NULL, individu_id INT DEFAULT NULL, INDEX IDX_6A2CA10CFD02F13 (evenement_id), INDEX IDX_6A2CA10C480B6028 (individu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE nom (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE prenom (id INT AUTO_INCREMENT NOT NULL, prenom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE relation (id INT AUTO_INCREMENT NOT NULL, premier_individu_id INT NOT NULL, deuxieme_individu_id INT NOT NULL, type_relation_id INT NOT NULL, INDEX IDX_6289474952C38D70 (premier_individu_id), INDEX IDX_62894749441719BA (deuxieme_individu_id), INDEX IDX_62894749794F46CA (type_relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE sexe (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE type_evenement (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE type_relation (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E131A4F72 FOREIGN KEY (commune_id) REFERENCES commune (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E88939516 FOREIGN KEY (type_evenement_id) REFERENCES type_evenement (id)');
        $this->addSql('ALTER TABLE individu ADD CONSTRAINT FK_5EE42FCE448F3B3C FOREIGN KEY (sexe_id) REFERENCES sexe (id)');
        $this->addSql('ALTER TABLE individu ADD CONSTRAINT FK_5EE42FCEC8121CE9 FOREIGN KEY (nom_id) REFERENCES nom (id)');
        $this->addSql('ALTER TABLE individu_prenom ADD CONSTRAINT FK_C3483963480B6028 FOREIGN KEY (individu_id) REFERENCES individu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE individu_prenom ADD CONSTRAINT FK_C348396358819F9E FOREIGN KEY (prenom_id) REFERENCES prenom (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE individu_evenement ADD CONSTRAINT FK_9F40312480B6028 FOREIGN KEY (individu_id) REFERENCES individu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE individu_evenement ADD CONSTRAINT FK_9F40312FD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10CFD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id)');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C480B6028 FOREIGN KEY (individu_id) REFERENCES individu (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_6289474952C38D70 FOREIGN KEY (premier_individu_id) REFERENCES individu (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749441719BA FOREIGN KEY (deuxieme_individu_id) REFERENCES individu (id)');
        $this->addSql('ALTER TABLE relation ADD CONSTRAINT FK_62894749794F46CA FOREIGN KEY (type_relation_id) REFERENCES type_relation (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement DROP FOREIGN KEY FK_B26681E131A4F72');
        $this->addSql('ALTER TABLE evenement DROP FOREIGN KEY FK_B26681E88939516');
        $this->addSql('ALTER TABLE individu DROP FOREIGN KEY FK_5EE42FCE448F3B3C');
        $this->addSql('ALTER TABLE individu DROP FOREIGN KEY FK_5EE42FCEC8121CE9');
        $this->addSql('ALTER TABLE individu_prenom DROP FOREIGN KEY FK_C3483963480B6028');
        $this->addSql('ALTER TABLE individu_prenom DROP FOREIGN KEY FK_C348396358819F9E');
        $this->addSql('ALTER TABLE individu_evenement DROP FOREIGN KEY FK_9F40312480B6028');
        $this->addSql('ALTER TABLE individu_evenement DROP FOREIGN KEY FK_9F40312FD02F13');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10CFD02F13');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10C480B6028');
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_6289474952C38D70');
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_62894749441719BA');
        $this->addSql('ALTER TABLE relation DROP FOREIGN KEY FK_62894749794F46CA');
        $this->addSql('DROP TABLE commune');
        $this->addSql('DROP TABLE evenement');
        $this->addSql('DROP TABLE individu');
        $this->addSql('DROP TABLE individu_prenom');
        $this->addSql('DROP TABLE individu_evenement');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE nom');
        $this->addSql('DROP TABLE prenom');
        $this->addSql('DROP TABLE relation');
        $this->addSql('DROP TABLE sexe');
        $this->addSql('DROP TABLE type_evenement');
        $this->addSql('DROP TABLE type_relation');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
